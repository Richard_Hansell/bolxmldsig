﻿namespace BOLXMLDSIG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.XMLContainer = new System.Windows.Forms.RichTextBox();
            this.SignXML = new System.Windows.Forms.Button();
            this.Flow = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadTemplate = new System.Windows.Forms.Button();
            this.BolIPLabel = new System.Windows.Forms.Label();
            this.Electricity = new System.Windows.Forms.RadioButton();
            this.Gas = new System.Windows.Forms.RadioButton();
            this.ValidateXML = new System.Windows.Forms.Button();
            this.ValidateXMLContainer = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // XMLContainer
            // 
            this.XMLContainer.Location = new System.Drawing.Point(12, 39);
            this.XMLContainer.Name = "XMLContainer";
            this.XMLContainer.Size = new System.Drawing.Size(776, 357);
            this.XMLContainer.TabIndex = 0;
            this.XMLContainer.Text = "";
            // 
            // SignXML
            // 
            this.SignXML.Location = new System.Drawing.Point(700, 444);
            this.SignXML.Name = "SignXML";
            this.SignXML.Size = new System.Drawing.Size(88, 23);
            this.SignXML.TabIndex = 1;
            this.SignXML.Text = "Sign and Send";
            this.SignXML.UseVisualStyleBackColor = true;
            this.SignXML.Click += new System.EventHandler(this.SignXML_Click);
            // 
            // Flow
            // 
            this.Flow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Flow.FormattingEnabled = true;
            this.Flow.Location = new System.Drawing.Point(215, 12);
            this.Flow.Name = "Flow";
            this.Flow.Size = new System.Drawing.Size(193, 21);
            this.Flow.Sorted = true;
            this.Flow.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Template";
            // 
            // LoadTemplate
            // 
            this.LoadTemplate.Location = new System.Drawing.Point(414, 10);
            this.LoadTemplate.Name = "LoadTemplate";
            this.LoadTemplate.Size = new System.Drawing.Size(75, 23);
            this.LoadTemplate.TabIndex = 4;
            this.LoadTemplate.Text = "Load";
            this.LoadTemplate.UseVisualStyleBackColor = true;
            this.LoadTemplate.Click += new System.EventHandler(this.LoadTemplate_Click);
            // 
            // BolIPLabel
            // 
            this.BolIPLabel.AutoSize = true;
            this.BolIPLabel.Location = new System.Drawing.Point(476, 449);
            this.BolIPLabel.Name = "BolIPLabel";
            this.BolIPLabel.Size = new System.Drawing.Size(218, 13);
            this.BolIPLabel.TabIndex = 5;
            this.BolIPLabel.Text = "Target IP = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            // 
            // Electricity
            // 
            this.Electricity.AutoSize = true;
            this.Electricity.Location = new System.Drawing.Point(12, 12);
            this.Electricity.Name = "Electricity";
            this.Electricity.Size = new System.Drawing.Size(70, 17);
            this.Electricity.TabIndex = 6;
            this.Electricity.Text = "Electricity";
            this.Electricity.UseVisualStyleBackColor = true;
            this.Electricity.CheckedChanged += new System.EventHandler(this.Electricity_CheckedChanged);
            // 
            // Gas
            // 
            this.Gas.AutoSize = true;
            this.Gas.Location = new System.Drawing.Point(88, 12);
            this.Gas.Name = "Gas";
            this.Gas.Size = new System.Drawing.Size(44, 17);
            this.Gas.TabIndex = 7;
            this.Gas.Text = "Gas";
            this.Gas.UseVisualStyleBackColor = true;
            this.Gas.CheckedChanged += new System.EventHandler(this.Gas_CheckedChanged);
            // 
            // ValidateXML
            // 
            this.ValidateXML.Location = new System.Drawing.Point(12, 439);
            this.ValidateXML.Name = "ValidateXML";
            this.ValidateXML.Size = new System.Drawing.Size(75, 23);
            this.ValidateXML.TabIndex = 8;
            this.ValidateXML.Text = "Validate";
            this.ValidateXML.UseVisualStyleBackColor = true;
            this.ValidateXML.Click += new System.EventHandler(this.ValidateXML_Click);
            // 
            // ValidateXMLContainer
            // 
            this.ValidateXMLContainer.Location = new System.Drawing.Point(93, 405);
            this.ValidateXMLContainer.Name = "ValidateXMLContainer";
            this.ValidateXMLContainer.Size = new System.Drawing.Size(377, 62);
            this.ValidateXMLContainer.TabIndex = 10;
            this.ValidateXMLContainer.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 479);
            this.Controls.Add(this.ValidateXMLContainer);
            this.Controls.Add(this.ValidateXML);
            this.Controls.Add(this.Gas);
            this.Controls.Add(this.Electricity);
            this.Controls.Add(this.BolIPLabel);
            this.Controls.Add(this.LoadTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Flow);
            this.Controls.Add(this.SignXML);
            this.Controls.Add(this.XMLContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "BOL XML Digitial Signing Service";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox XMLContainer;
        private System.Windows.Forms.Button SignXML;
        private System.Windows.Forms.ComboBox Flow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadTemplate;
        private System.Windows.Forms.Label BolIPLabel;
        private System.Windows.Forms.RadioButton Electricity;
        private System.Windows.Forms.RadioButton Gas;
        private System.Windows.Forms.Button ValidateXML;
        private System.Windows.Forms.RichTextBox ValidateXMLContainer;
    }
}

