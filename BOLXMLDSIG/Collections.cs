﻿using System;

namespace BOLXMLDSIG
{
    class Collections
    {
        public BOLRequestType CreateECOSG_IN_01ServiceRequest(string MPAN, string MSN, string Mode, string KeyDate)
        {
            //Service Request ECOSG_IN_01
            var e = new ECOSG_IN_01_TYPE();
            e.ImportMPAN = MPAN;
            e.MSN = MSN;
            e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.COTFlag = false;
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";
            e.VulnerabilityIndicator = false;
            e.SSC = "0151";

            //Payment mode
            e.PaymentMode = new ECOSG_IN_01_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new ECOSG_IN_01_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtEmergency = true,
                    DebtRecoveryRateCap = 14000, //£140.00
                    EmergencyCreditLimit = "500", //£5.00
                    EmergencyCreditThreshold = "200", //£2.00
                    MaxMeterBalance = "100000",
                    NonDisablementCalendarID = "0",
                    MaxCreditThreshold = "25000"
                };

            //Tariff information includes registers
            var mt = new ECOSG_IN_01_TYPEMeterTariff();
            mt.StandingCharge = 14;
            var rd = new ECOSG_IN_01_TYPEMeterTariffRegisterDefinitions[2];
            var r1 = new ECOSG_IN_01_TYPEMeterTariffRegisterDefinitions();
            r1.RegisterID = "01";
            r1.RegisterPrice = 9;
            r1.MeasurementQuantityID = "AI";
            r1.MeterRegisterMultiplier = "1";
            r1.TPR = "00210";
            rd[0] = r1;
            var r2 = new ECOSG_IN_01_TYPEMeterTariffRegisterDefinitions();
            r2.RegisterID = "02";
            r2.RegisterPrice = 18;
            r2.MeasurementQuantityID = "AI";
            r2.MeterRegisterMultiplier = "1";
            r2.TPR = "00043";
            rd[1] = r2;
            mt.RegisterDefinitions = rd;
            e.MeterTariff = mt;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.ECOSG,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateEINIM_IN_01ServiceRequest(string MPAN, string Mode, string KeyDate)
        {
            //Service Request EINIM_IN_01
            var e = new EINIM_IN_01_TYPE();
            e.ImportMPAN = MPAN;
            e.BillingTime = Convert.ToDateTime("00:00:00");
            e.DisplayMessage = "Welcome to Spark Energy";
            e.ItemElementName = ItemChoiceType56.BillingFrequencyMonthly;
            e.Item = "1";

            //DSP Schedule
            var dsp = new EINIM_IN_01_TYPEDSPScheduleRequest[1] {
                new EINIM_IN_01_TYPEDSPScheduleRequest() {
                ScheduleFrequency = EINIM_IN_01_TYPEDSPScheduleRequestScheduleFrequency.Daily,
                ServiceReferenceVariant = Mode == "Credit" ? ElecDSPScheduledServiceReferenceVariant.Item461 : ElecDSPScheduledServiceReferenceVariant.Item414,
                ScheduleStartDate = Convert.ToDateTime(KeyDate),
                ScheduleEndDateSpecified = false,
                ScheduleExecutionStartTime = Convert.ToDateTime("00:00:00") }
            };
            e.DSPScheduleRequest = dsp;

            //Alerts
            var a = new EINIM_IN_01_TYPEAlertBehaviours();
            a.CombinedCreditBelowThreshold = AlertStatusType.Disable;
            a.CreditAddedLocally = AlertStatusType.Disable;
            a.DeviceJoinedSMHAN = AlertStatusType.Disable;
            a.EmergencyCreditAvailable = AlertStatusType.Disable;
            e.AlertBehaviours = a;

            //UDC Load Limiting (we don't use this so pass an empty container)

            //UDC Power Threshold
            var u = new EINIM_IN_01_TYPEUDCPowerThreshold();
            u.LowMediumPowerThreshold = 5;
            u.MediumHighPowerThreshold = 20;
            e.UDCPowerThreshold = u;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EINIM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGCOSG_IN_01ServiceRequest(string MPRN, string MSN, string Mode, string KeyDate)
        {
            //Service Request GCOSG_IN_01
            var e = new GCOSG_IN_01_TYPE();
            e.ImportMPRN = MPRN;
            e.MSN = MSN;
            e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.COTFlag = false;
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";
            e.VulnerabilityIndicator = false;

            //Payment Mode
            e.PaymentMode = new GCOSG_IN_01_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new GCOSG_IN_01_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtEmergency = true,
                    DebtRecoveryRateCap = 14000, //£140.00
                    EmergencyCreditLimit = "500",
                    EmergencyCreditThreshold = "200",
                    MaxMeterBalance = "100000", //£1000.00
                    NonDisablementCalendarID = "0",
                    MaxCreditThreshold = "25000" //£250.00
                };

            //Tariff information includes registers
            var mt = new GCOSG_IN_01_TYPEMeterTariff();
            mt.StandingCharge = 14;
            var rd = new GCOSG_IN_01_TYPEMeterTariffRegisterDefinitions[1];
            var r1 = new GCOSG_IN_01_TYPEMeterTariffRegisterDefinitions();
            r1.RegisterID = "01";
            r1.RegisterPrice = 9;
            rd[0] = r1;
            mt.RegisterDefinitions = rd;
            e.MeterTariff = mt;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GCOSG,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGINIM_IN_01ServiceRequest(string MPRN, string Mode, string KeyDate)
        {
            //Service Request GINIM_IN_01
            var e = new GINIM_IN_01_TYPE();
            e.ImportMPRN = MPRN;
            e.BillingPeriodStart = Convert.ToDateTime("00:00:00");
            e.Periodicity = GINIM_IN_01_TYPEPeriodicity.Monthly;
            e.DisplayMessage = "Welcome to Spark Energy";

            //DSP Schedule
            var dsp = new GINIM_IN_01_TYPEDSPSchedule[1] {
                new GINIM_IN_01_TYPEDSPSchedule() {
                    Frequency = GINIM_IN_01_TYPEDSPScheduleFrequency.Daily,
                    ServiceReferenceVariant = Mode == "Credit" ? GasDSPScheduledServiceReferenceVariant.Item461 : GasDSPScheduledServiceReferenceVariant.Item414,
                    ScheduleStartDate = Convert.ToDateTime(KeyDate),
                    ScheduleEndDateSpecified = false,
                    ScheduleExecutionStartTime = Convert.ToDateTime("00:00:00")
                }
            };
            e.DSPSchedule = dsp;

            //Alerts
            var a = new GINIM_IN_01_TYPEAlertBehaviours();
            a.CombinedCreditBelowThreshold = AlertStatusType.Disable;
            a.CreditAddedLocally = AlertStatusType.Disable;
            a.DeviceJoinedSMHAN = AlertStatusType.Disable;
            a.EmergencyCreditAvailable = AlertStatusType.Disable;
            a.RemainingBatteryCapacityReset = AlertStatusType.Enable;
            a.RemainingBatteryCapacityResetSpecified = true;
            a.ValveTested = AlertStatusType.Enable;
            a.ValveTestedSpecified = true;
            e.AlertBehaviours = a;

            //UDC Load Limiting (we don't use this so pass an empty container)

            //UDC Gas Conversion
            var u = new GINIM_IN_01_TYPEUDCGasConversion();
            u.CalorificValue = (decimal)39.2;
            u.ConversionFactor = (decimal)1.02264;
            e.UDCGasConversion = u;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GINIM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCMOD_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string Mode, string KeyDate)
        {
            //Service Request XCMOD
            var e = new XCMOD_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType64.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType64.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.PaymentMode = new XCMOD_IN_01_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new XCMOD_IN_01_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtDisabledSpecified = true,
                    SuspendDebtEmergency = true,
                    SuspendDebtEmergencySpecified = true,
                    DebtRecoveryRateCap = (ushort)14000, //£140.00
                    EmergencyCreditLimit = "500",
                    EmergencyCreditThreshold = "200",
                    MaxMeterBalance = "10000",
                    NonDisablementCalendarID = "",
                    MaxCreditThreshold = "25000",
                    VulnerabilityIndicator = false
                };
            e.DeleteSchedule = true;
            //Do we need to provide the schedule ids to be deleted, or it will just delete all of them if this is blank?
            //e.DeleteScheduleSpecified = ??;
            var d = new XCMOD_IN_01_TYPESchedule[1] {
                new XCMOD_IN_01_TYPESchedule() {
                    ScheduledFrequency = XCMOD_IN_01_TYPEScheduleScheduledFrequency.Daily,
                    ServiceReferenceVariant = Mode == "Credit" ? DSPScheduledServiceReferenceVariant.Item461 : DSPScheduledServiceReferenceVariant.Item414,
                    ScheduleStartDate = Convert.ToDateTime(KeyDate),
                    ScheduleEndDateSpecified = false,
                    ScheduleExecutionStartTime = Convert.ToDateTime("00:00:00") }
            };
            e.Schedule = d;
            var s = new XCMOD_IN_01_TYPESupplierInformation();
            s.SupplierName = "Spark";
            s.SupplierTelephoneNumber = "03333216246";
            e.SupplierInformation = s;
            e.DisplayMessage = "Hi, we updated your payment mode!";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCMOD,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateECOSL_IN_01ServiceRequest()
        {
            //Service Request ECOSL - this is triggered by AFMS
            var e = new ECOSL_IN_01_TYPE();

            //Create a new service request
            var br = new BOLRequestType { };
            return br;
        }

        public BOLRequestType CreateGCOSL_IN_01ServiceRequest()
        {
            //Service Request GCOSL - this is triggered by AFMS
            var e = new GCOSL_IN_01_TYPE();

            //Create a new service request
            var br = new BOLRequestType { };
            return br;
        }

        public BOLRequestType CreateENABL_IN_01ServiceRequest(string MPAN, string MSN)
        {
            //Service Request ENABL
            var e = new ENABL_IN_01_TYPE();
            e.ImportMPAN = MPAN;
            e.MSN = MSN;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.ENABL,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateECOTN_IN_01ServiceRequest(string MPAN, string MSN, string Mode, string KeyDate)
        {
            //Service Request ECOTN_IN_01
            var e = new ECOTN_IN_01_TYPE();
            e.ImportMPAN = MPAN;
            e.MSN = MSN;
            e.MoveDate = Convert.ToDateTime(KeyDate);
            e.NewTenantKnownFlag = true;
            e.PaymentMode = new ECOTN_IN_01_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new ECOTN_IN_01_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtDisabledSpecified = true,
                    SuspendDebtEmergency = true,
                    SuspendDebtEmergencySpecified = true,
                    EmergencyCreditLimit = 500, //£5.00
                    EmergencyCreditThreshold = 200, //£2.00
                    MaxMeterBalance = 100000, //£1,000.00
                    NonDisablementCalendarID = "17/18-8to8",
                    MaxCreditThreshold = 25000, //£250.00
                    VulnerabilityIndicator = false
                };
            e.DisplayMessage = "Welcome to Spark Energy";
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";

            //Debt Recovery
            if (Mode == "Prepayment")
            {
                var d = new ECOTN_IN_01_TYPEDebtRecovery();
                d.DebtRecoveryRate1 = (short)500;
                d.DebtRecoveryRate1Period = ECOTN_IN_01_TYPEDebtRecoveryDebtRecoveryRate1Period.WEEKLY;
                d.DebtRecoveryRate2 = (short)0;
                d.DebtRecoveryRate2Period = ECOTN_IN_01_TYPEDebtRecoveryDebtRecoveryRate2Period.WEEKLY;
                d.PaymentDebtRegister = 0;
                d.TimeDebtRegister1 = 0;
                d.TimeDebtRegister2 = 0;
                e.DebtRecovery = d;
            }

            //Schedule
            var s = new ECOTN_IN_01_TYPESchedule[1];
            var s1 = new ECOTN_IN_01_TYPESchedule();
            s1.Frequency = ECOTN_IN_01_TYPEScheduleFrequency.DAILY;
            if (Mode == "Credit")
                s1.ServiceReferenceVariant = ElecDSPScheduledServiceReferenceVariant.Item461;
            else
                s1.ServiceReferenceVariant = ElecDSPScheduledServiceReferenceVariant.Item414;
            s1.StartDate = Convert.ToDateTime(KeyDate);
            s1.EndDateSpecified = false;
            s1.ExecutionStartTime = Convert.ToDateTime("00:00:00");
            s[0] = s1;
            e.Schedule = s;

            //Billing Calendar
            e.BillingCalendar = new ECOTN_IN_01_TYPEBillingCalendar();
            e.BillingCalendar.BillingTime = Convert.ToDateTime("00:00:00");
            e.BillingCalendar.ItemElementName = ItemChoiceType55.BillingFrequencyMonthly;
            e.BillingCalendar.Item = "01";

            //Tariff information includes registers
            var mt = new ECOTN_IN_01_TYPETariff();
            mt.SSC = "0151";
            mt.StandingCharge = 14;
            var rd = new ECOTN_IN_01_TYPETariffRegisterDefinitions[2];
            var r1 = new ECOTN_IN_01_TYPETariffRegisterDefinitions();
            r1.RegisterId = "01";
            r1.RegisterPrice = 9;
            r1.TPR = "00210";
            rd[0] = r1;
            var r2 = new ECOTN_IN_01_TYPETariffRegisterDefinitions();
            r2.RegisterId = "02";
            r2.RegisterPrice = 18;
            r2.TPR = "00043";
            rd[1] = r2;
            mt.RegisterDefinitions = rd;
            e.Tariff = mt;

            //Prices are optional, and we already set the price in the register definitions
            //var p = new ECOTN_IN_01_TYPEPrice();
            //p.
            //e.Price = p;

            //Power Threshold
            e.PowerThreshold = new ECOTN_IN_01_TYPEPowerThreshold();
            e.PowerThreshold.LowMediumPowerThreshold = 5;
            e.PowerThreshold.MediumHighPowerThreshold = 20;

            //Last fields
            e.MeterType = "S2";
            e.MeasurementQuantityID = "AI";
            e.MeterRegisterMultiplier = 1;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.ECOTN,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGCOTN_IN_01ServiceRequest(string MPRN, string MSN, string Mode, string KeyDate)
        {
            //Service Request GCOTN_IN_01
            var e = new GCOTN_IN_01_TYPE();
            e.ImportMPRN = MPRN;
            e.MSN = MSN;
            e.MoveDate = Convert.ToDateTime(KeyDate);
            e.NewTenantKnownFlag = true;
            e.PaymentMode = new GCOTN_IN_01_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new GCOTN_IN_01_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtDisabledSpecified = true,
                    SuspendDebtEmergency = true,
                    SuspendDebtEmergencySpecified = true,
                    EmergencyCreditLimit = 500, //£5.00
                    EmergencyCreditThreshold = 200, //£2.00
                    MaxMeterBalance = 100000, //£1,000.00
                    NonDisablementCalendarID = "17/18-8to8",
                    MaxCreditThreshold = 25000, //£250.00
                    VulnerabilityIndicator = false
                };
            e.DisplayMessage = "Welcome to Spark Energy";
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";

            //Debt Recovery
            if (Mode == "Prepayment")
            {
                var d = new GCOTN_IN_01_TYPEDebtRecovery();
                d.DebtRecoveryRate1 = (short)71;
                d.DebtRecoveryRate1Period = GCOTN_IN_01_TYPEDebtRecoveryDebtRecoveryRate1Period.DAILY;
                d.DebtRecoveryRate2 = (short)0;
                d.DebtRecoveryRate2Period = GCOTN_IN_01_TYPEDebtRecoveryDebtRecoveryRate2Period.DAILY;
                d.PaymentDebtRegister = 0;
                d.TimeDebtRegister1 = 0;
                d.TimeDebtRegister2 = 0;
                e.DebtRecovery = d;
            }

            //Schedule
            var s = new GCOTN_IN_01_TYPESchedule[1];
            var s1 = new GCOTN_IN_01_TYPESchedule();
            s1.Frequency = GCOTN_IN_01_TYPEScheduleFrequency.DAILY;
            if (Mode == "Credit")
                s1.ServiceReferenceVariant = GasDSPScheduledServiceReferenceVariant.Item461;
            else
                s1.ServiceReferenceVariant = GasDSPScheduledServiceReferenceVariant.Item414;
            s1.StartDate = Convert.ToDateTime(KeyDate);
            s1.EndDateSpecified = false;
            s[0] = s1;
            e.Schedule = s;

            //Billing Calendar
            e.BillingCalendar = new GCOTN_IN_01_TYPEBillingCalendar();
            e.BillingCalendar.BillingPeriodStart = Convert.ToDateTime(KeyDate + " 00:00:00");
            e.BillingCalendar.Periodicity = GCOTN_IN_01_TYPEBillingCalendarPeriodicity.MONTHLY;

            //Tariff information includes registers
            var mt = new GCOTN_IN_01_TYPETariff();
            mt.StandingCharge = 14;
            var rd = new GCOTN_IN_01_TYPETariffRegisterDefinitions[1];
            var r1 = new GCOTN_IN_01_TYPETariffRegisterDefinitions();
            r1.RegisterId = "01";
            r1.RegisterPrice = 9;
            rd[0] = r1;
            mt.RegisterDefinitions = rd;
            e.Tariff = mt;

            //Price we leave blank

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GCOTN,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateEICOM_AI_01ServiceRequest(string MPAN, string MSN, string Mode, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request EICOM_AI_01
            var e = new EICOM_AI_01_TYPE();
            e.ImportMPAN = MPAN;
            e.AerialInstall = false;
            e.AdditionalInformation = "Had a nice cup of tea";
            e.CHFDeviceID = CHFId;
            e.CHFLocation = CHFLocation.Deepindoors;
            e.DeviceID = DeviceId;
            e.EmergencyCreditBalance = "0";
            e.ExportMPAN = "";
            e.GISData = "";
            e.InstalledDevice = new EICOM_AI_01_TYPEInstalledDevice { InstalledMSN = MSN };
            e.JoinTimePeriod = "";
            e.MeterBalance = "0";
            e.MOPID = "Apid";
            if (Mode == "Prepayment")
                e.NewPaymentMode = new EICOM_AI_01_TYPENewPaymentMode { Item = "PaymentModePrepayment", ItemElementName = ItemChoiceType58.PaymentModePrepayment };
            else
                e.NewPaymentMode = new EICOM_AI_01_TYPENewPaymentMode { Item = "PaymentModeCredit", ItemElementName = ItemChoiceType58.PaymentModeCredit };
            e.OldPaymentMode = new EICOM_AI_01_TYPEOldPaymentMode { Item = "PaymentModeCredit", ItemElementName = ItemChoiceType57.PaymentModeCredit };
            e.ReadingsDateTime = DateTime.Now;
            e.ReadingsDateTimeSpecified = false;
            e.RemovedDevice = new EICOM_AI_01_TYPERemovedDevice { RemovedMSN = "" };
            e.SecondaryImportMPAN = MPAN;
            e.ExportMPAN = MPAN;
            e.SupplierMPID = "Elec";
            e.TransactionReference = "XXX";
            e.UserRefID = "JOEB";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EICOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGICOM_AI_01ServiceRequest(string MPRN, string MSN, string Mode, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request GICOM_AI_01
            var e = new GICOM_AI_01_TYPE();
            e.ImportMPRN = MPRN;
            e.AerialInstall = false;
            e.AdditionalInformation = "Had a nice cup of tea";
            e.CHFDeviceID = CHFId;
            e.CHFLocation = CHFLocation.Deepindoors;
            e.DeviceID = DeviceId;
            e.EmergencyCreditBalance = "0";
            e.GPFDeviceID = GPFId;
            e.GISData = "";
            e.InstalledDevice = new GICOM_AI_01_TYPEInstalledDevice { InstalledMSN = MSN };
            e.JoinTimePeriod = "";
            e.MeterBalance = "0";
            e.MAMID = "SMS";
            if (Mode == "Prepayment")
                e.NewPaymentMode = new GICOM_AI_01_TYPENewPaymentMode { Item = "PaymentModePrepayment", ItemElementName = ItemChoiceType60.PaymentModePrepayment };
            else
                e.NewPaymentMode = new GICOM_AI_01_TYPENewPaymentMode { Item = "PaymentModeCredit", ItemElementName = ItemChoiceType60.PaymentModeCredit };
            e.OldPaymentMode = new GICOM_AI_01_TYPEOldPaymentMode { Item = "PaymentModeCredit", ItemElementName = ItemChoiceType59.PaymentModeCredit };
            e.ReadingsDateTime = DateTime.Now;
            e.ReadingsDateTimeSpecified = false;
            e.RemovedDevice = new GICOM_AI_01_TYPERemovedDevice { RemovedMSN = "" };
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "XXX";
            e.UserRefID = "JOEB";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GICOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateEICOM_IN_04ServiceRequest(string MPAN, string MSN, string Mode, string KeyDate)
        {
            //Service Request EICOM_IN_04
            var e = new EICOM_IN_04_TYPE();
            e.ImportMPAN = MPAN;
            e.InstallationDate = Convert.ToDateTime(KeyDate);
            e.InstallationDateSpecified = true;
            e.PaymentMode = new EICOM_IN_04_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new EICOM_IN_04_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtDisabledSpecified = true,
                    SuspendDebtEmergency = true,
                    SuspendDebtEmergencySpecified = true,
                    EmergencyCreditLimit = 500, //£5.00
                    EmergencyCreditThreshold = 200, //£2.00
                    MaxMeterBalance = 100000, //£1,000.00
                    NonDisablementCalendarID = "17/18-8to8",
                    MaxCreditThreshold = 25000, //£250.00
                    VulnerabilityIndicator = false
                    //Top up amount not specified
                };
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";

            //Tariff information includes registers
            var mt = new EICOM_IN_04_TYPETariff();
            mt.SSC = "0151";
            mt.StandingCharge = 14;
            var rd = new EICOM_IN_04_TYPETariffRegisterDefinitions[2];
            var r1 = new EICOM_IN_04_TYPETariffRegisterDefinitions();
            r1.RegisterID = "01";
            r1.RegisterPrice = 9;
            r1.TPR = "00210";
            rd[0] = r1;
            var r2 = new EICOM_IN_04_TYPETariffRegisterDefinitions();
            r2.RegisterID = "02";
            r2.RegisterPrice = 18;
            r2.TPR = "00043";
            rd[1] = r2;
            mt.RegisterDefinitions = rd;
            mt.StartDateTime = Convert.ToDateTime(KeyDate);
            mt.StartDateTimeSpecified = true;
            e.Tariff = mt;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EICOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGICOM_IN_04ServiceRequest(string MPRN, string MSN, string Mode, string KeyDate)
        {
            //Service Request GICOM_AI_01
            var e = new GICOM_IN_04_TYPE();
            e.ImportMPRN = MPRN;
            e.InstallationDate = Convert.ToDateTime(KeyDate);
            e.InstallationDateSpecified = true;
            e.PaymentMode = new GICOM_IN_04_TYPEPaymentMode();
            if (Mode == "Credit")
                e.PaymentMode.Item = "PaymentModeCredit";
            else
                e.PaymentMode.Item = new GICOM_IN_04_TYPEPaymentModePaymentModePrepayment
                {
                    DisablementThreshold = "0",
                    SuspendDebtDisabled = true,
                    SuspendDebtDisabledSpecified = true,
                    SuspendDebtEmergency = true,
                    SuspendDebtEmergencySpecified = true,
                    EmergencyCreditLimit = 500, //£5.00
                    EmergencyCreditThreshold = 200, //£2.00
                    MaxMeterBalance = 100000, //£1,000.00
                    NonDisablementCalendarID = "17/18-8to8",
                    MaxCreditThreshold = 25000, //£250.00
                    VulnerabilityIndicator = false
                }; e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";
            e.LDZIdentifier = "SC";

            //Tariff information includes registers
            var mt = new GICOM_IN_04_TYPETariff();
            mt.StandingCharge = 14;
            var rd = new GICOM_IN_04_TYPETariffRegisterDefinitions[1];
            var r1 = new GICOM_IN_04_TYPETariffRegisterDefinitions();
            r1.RegisterID = "01";
            r1.RegisterPrice = 9;
            rd[0] = r1;
            mt.RegisterDefinitions = rd;
            e.Tariff = mt;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GICOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXITTD_AI_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId, string PPMId)
        {
            //Service Request XITTD_AI_01
            var e = new XITTD_AI_01_TYPE();
            e.DeviceID = PPMId;
            e.CHFDeviceID = CHFId;
            if (FuelType == "E")
                e.SupplierMPID = "Elec";
            else
                e.SupplierMPID = "Gas";
            e.TransactionReference = "REF99";
            e.Items = new string[2];
            e.ItemsElementName = new ItemsChoiceType9[2];

            //Items 0 is the MPXN
            if (FuelType == "E")
                e.ItemsElementName[0] = ItemsChoiceType9.ImportMPAN;
            else
                e.ItemsElementName[0] = ItemsChoiceType9.ImportMPRN;
            e.Items[0] = MPXN;

            //Items 1 is the MOPID or MAMID
            if (FuelType == "E")
            {
                e.ItemsElementName[1] = ItemsChoiceType9.MOPID;
                e.Items[1] = "SIEM";
            }
            else
            {
                e.ItemsElementName[1] = ItemsChoiceType9.MAMID;
                e.Items[1] = "SMS";
            }

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XITTD,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXIPMD_AI_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId, string PPMId)
        {
            //Service Request XIPMD_AI_01
            var e = new XIPMD_AI_01_TYPE();
            e.CHFDeviceID = CHFId;
            e.DeviceID = PPMId;
            if (FuelType == "E")
                e.SupplierMPID = "Elec";
            else
                e.SupplierMPID = "Gas";
            e.TransactionReference = "REF01";
            e.Items = new string[2];
            e.ItemsElementName = new ItemsChoiceType8[2];

            //Items 0 is the MPXN
            if (FuelType == "E")
                e.ItemsElementName[0] = ItemsChoiceType8.ImportMPAN;
            else
                e.ItemsElementName[0] = ItemsChoiceType8.ImportMPRN;
            e.Items[0] = MPXN;

            //Items 1 is the MOPID or MAMID
            if (FuelType == "E")
            {
                e.ItemsElementName[1] = ItemsChoiceType8.MOPID;
                e.Items[1] = "SIEM";
            }
            else
            {
                e.ItemsElementName[1] = ItemsChoiceType8.MAMID;
                e.Items[1] = "SMS";
            }

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XIPMD,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateEVCOM_AI_01ServiceRequest(string MPAN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request EVCOM_AI_01
            var e = new EVCOM_AI_01_TYPE();
            e.CHFDeviceID = CHFId;
            e.DeviceID = DeviceId;
            e.SupplierMPID = "SPARK";
            e.CommsHub = new EVCOM_AI_01_TYPECommsHub
            {
                AerialInstall = false,
                AdditionalInformation = "",
                CHFInstallType = CHFInstallType.NewCHFInstall,
                CHFLocation = CHFLocation.Deepindoors,
                ConnectivityObstructionCheck = true,
                GISData = "",
                MetalObstructionCheck = true,
                PremiseType = PremiseType.DetachedSemiDetached,
                SharedObstructionCheck = true,
                UserRefID = "SAMS"
            };
            e.EmergencyCreditBalance = "0";
            e.ImportMPAN = MPAN;
            e.InstallDateTime = DateTime.Now;
            var r = new EVCOM_AI_01_TYPEInstalledDeviceInstalledReadings
            {
                RegisterID = "1",
                RegisterReading = (decimal)100.2
            };
            e.InstalledDevice = new EVCOM_AI_01_TYPEInstalledDevice
            {
                InstalledMSN = MSN,
                InstalledReadings = new EVCOM_AI_01_TYPEInstalledDeviceInstalledReadings[1] { r }
            };
            e.MeterBalance = "10";
            e.MOPID = "SIEM";
            e.PaymentMode = "PaymentModeCredit";
            var x = new EVCOM_AI_01_TYPERemovedDeviceRemovedReadings
            {
                RemovedRegisterID = "2",
                RegisterReading = (decimal)500.4
            };
            e.RemovedDevice = new EVCOM_AI_01_TYPERemovedDevice
            {
                RemovedMSN = "XYZ12345",
                RemovedReadings = new EVCOM_AI_01_TYPERemovedDeviceRemovedReadings[1] { x }
            };
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "TRANSREF1234";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EVCOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGVCOM_AI_01ServiceRequest(string MPRN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request GVCOM_AI_01
            var e = new GVCOM_AI_01_TYPE();
            e.CHFDeviceID = CHFId;
            e.DeviceID = DeviceId;
            e.SupplierMPID = "SPARK";
            e.CommsHub = new GVCOM_AI_01_TYPECommsHub
            {
                AerialInstall = false,
                AdditionalInformation = "",
                CHFInstallType = CHFInstallType.NewCHFInstall,
                CHFLocation = CHFLocation.Deepindoors,
                ConnectivityObstructionCheck = true,
                GISData = "",
                MetalObstructionCheck = true,
                PremiseType = PremiseType.DetachedSemiDetached,
                SharedObstructionCheck = true,
                UserRefID = "SAMS"
            };
            e.EmergencyCreditBalance = "0";
            e.ImportMPRN = MPRN;
            e.InstallDateTime = DateTime.Now;
            var r = new GVCOM_AI_01_TYPEInstalledDeviceInstalledReadings
            {
                RegisterID = "1",
                RegisterReading = (decimal)100.2
            };
            e.InstalledDevice = new GVCOM_AI_01_TYPEInstalledDevice
            {
                InstalledMSN = MSN,
                InstalledReadings = new GVCOM_AI_01_TYPEInstalledDeviceInstalledReadings[1] { r }
            };
            e.MeterBalance = "10";
            e.MAMID = "SMS";
            e.PaymentMode = "PaymentModeCredit";
            e.RemovedDevice = new GVCOM_AI_01_TYPERemovedDevice
            {
                RemovedMSN = "XYZ12345",
                RegisterReading = (decimal)1234.6,
                RegisterReadingSpecified = true
            };
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "TRANSREF1234";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GVCOM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCHUB_AI_01ServiceRequest(string MPAN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request XCHUB_AI_01
            var e = new XCHUB_AI_01_TYPE();
            e.MOPID = "SIEM";
            e.ImportMPAN = MPAN;
            var c = new XCHUB_AI_01_TYPECHFDataCHFFaultReturnType();
            var r = new XCHUB_AI_01_TYPECHFDataCHFFaultReason();
            e.CHFData = new XCHUB_AI_01_TYPECHFData
            {
                AdditionalInformation = "Meter is going mouldy",
                UserRefID = "JOHNS",
                CHFExchange = false,
                CHFDeviceID = CHFId,
                CHFConnectionMethod = XCHUB_AI_01_TYPECHFDataCHFConnectionMethod.ESME,
                GPFDeviceID = GPFId,
                Items = new object[2] { c, r }
            };
            e.RemovalDateTime = DateTime.Now;
            e.NewCHFData = new XCHUB_AI_01_TYPENewCHFData
            {
                AdditionalInformation = "Shiny new meter",
                AerialInstall = false,
                CHFDeviceID = CHFId,
                CHFInstallType = XCHUB_AI_01_TYPENewCHFDataCHFInstallType.NewCHFInstall,
                CHFLocation = XCHUB_AI_01_TYPENewCHFDataCHFLocation.BasementorCellar,
                GISData = "",
                GPFDeviceID = GPFId
            };
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "TRANSREF1234";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCHUB,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXVALI_AI_01ServiceRequest(string MPXN)
        {
            //Service Request XVALI_AI_01
            var e = new XVALI_AI_01_TYPE();
            e.AppointmentDateTime = DateTime.Now;
            var j = new XVALI_AI_01_TYPEJob
            {
                JobType = XVALI_AI_01_TYPEJobJobType.Investigation,
                ItemsElementName = new ItemsChoiceType10[2] { ItemsChoiceType10.MOPID, ItemsChoiceType10.ImportMPAN },
                Items = new string[2] { "SIEM", MPXN },
                SupplierMPID = "SPARK"
            };
            e.Job = new XVALI_AI_01_TYPEJob[1] { j };
            e.PropertyFilter = new PropertyFilter
            {
                AddressIdentifier = "34 Briarwood Avenue",
                Postcode = "TD7 5DP"
            };
            e.TransactionReference = "TRANSREF999";
            e.UPRN = "1234";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XVALI,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCANC_AI_01ServiceRequest(string FuelType, string MPXN)
        {
            //Service Request XCANC_AI_01
            var e = new XCANC_AI_01_TYPE();
            e.AbortReason = "House demolished";
            var a = new XCANC_AI_01_TYPEAgent
            {
                SupplierMPID = "SPARK",
                Item = FuelType == "E" ? "SIEM" : "SMS",
                ItemElementName = FuelType == "E" ? ItemChoiceType69.MOPID : ItemChoiceType69.MAMID
            };
            e.Agent = new XCANC_AI_01_TYPEAgent[1] { a };
            e.ItemElementName = FuelType == "E" ? ItemChoiceType68.ImportMPAN : ItemChoiceType68.ImportMPRN;
            e.Item = MPXN;
            e.AppointmentDateTime = DateTime.Now;
            e.JobAbort = true;
            e.PropertyFilter = new PropertyFilter
            {
                AddressIdentifier = "34 Briarwood Avenue",
                Postcode = "TD7 5DP"
            };
            e.TransactionReference = "TRANSXYZ123";
            e.UPRN = "1234";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCANC,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCHRT_AI_01ServiceRequest(string DeviceId, string GPFId, string CHFId)
        {
            //Service Request XCHRT_AI_01
            var e = new XCHRT_AI_01_TYPE();
            e.AdditionalInformation = "";
            e.CHFConnectionMethod = XCHRT_AI_01_TYPECHFConnectionMethod.ESME;
            e.CHFDeviceID = CHFId;
            e.GPFDeviceID = GPFId;
            e.IncidentReference = "INCIDENT101";
            e.MOPID = "SIEM";
            e.SupplierMPID = "SPARK";
            e.UserRefID = "MARKE";
            e.OtherDeviceID = DeviceId;
            var c = new XCHRT_AI_01_TYPECHFFaultReturnType();
            var r = new XCHRT_AI_01_TYPECHFFaultReason();
            e.Items = new object[2] { c, r };

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCHRT,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        //public BOLRequestType CreateXDPRN_IN_01ServiceRequest(string DeviceId)
        //{
        //    //Service Request XDPRN_IN_01
        //    var e = new XDPRN_IN_01_TYPE();
        //    e.DeviceFirmwareVersion = "1.18";
        //    e.DeviceID = DeviceId;
        //    e.DeviceManufacturer = "Liberty";
        //    e.DeviceModel = "Sparkatron 2000";
        //    e.DeviceType = "Smart Meter";
        //    e.ESMEVariant = ESMEVariant.ADE;
        //    e.ESMEVariantSpecified = true;
        //    e.SMETSCHTSVersion = "1.21.9";

        //    //Create a new service request
        //    var br = new BOLRequestType
        //    {
        //        Header = new UtilisoftRequestHeaderType
        //        {
        //            BusinessScenarioID = BusinessScenarioID.XDPRN,
        //            PrecedingScenarioID = null,
        //            Priority = null,
        //            SourceID = "SparkEnergy",
        //            SourceCounter = Guid.NewGuid().ToString()
        //        },
        //        Body = new BOLRequestTypeBody
        //        {
        //            Items = new object[] { e }
        //        }
        //    };
        //    return br;
        //}

        public BOLRequestType CreateEMREM_AI_01ServiceRequest(string MPAN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request EMREM_AI_01
            var e = new EMREM_AI_01_TYPE();
            e.CHFDeviceID = CHFId;
            e.ImportMPAN = MPAN;
            e.ExportMPAN = "123456789012";
            e.MeterExchange = true;
            e.MOPID = "SIEM";
            var r = new EMREM_AI_01_TYPERemovedDeviceRemovedReadings
            {
                RemovedRegisterID = "2",
                RegisterReading = (decimal)999.99
            };
            e.RemovedDevice = new EMREM_AI_01_TYPERemovedDevice
            {
                ActiveImportRegister = "1",
                AssetReusable = true,
                DeviceID = DeviceId,
                PrepaymentData = new EMREM_AI_01_TYPERemovedDevicePrepaymentData
                {
                    AccumulatedDebtRegister = "2",
                    EmergencyCreditBalance = "10",
                    MeterBalance = "100",
                    PaymentDebtRegister = "5",
                    TimeDebtRegister1 = "6",
                    TimeDebtRegister2 = "7"
                },
                ReadingDateTime = DateTime.Now,
                ReadingDateTimeSpecified = false,
                RemovedMSN = MSN,
                RemovedReadings = new EMREM_AI_01_TYPERemovedDeviceRemovedReadings[1] { r }
            };
            e.SecondaryImportMPAN = "1111111111111";
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "TRANS256";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EMREM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGMREM_AI_01ServiceRequest(string MPRN, string MSN, string KeyDate, string DeviceId, string GPFId, string CHFId)
        {
            //Service Request GMREM_AI_01
            var e = new GMREM_AI_01_TYPE();
            e.CHFDeviceID = CHFId;
            e.ImportMPRN = MPRN;
            e.MeterExchange = true;
            e.MAMID = "SMS";
            var r = new GMREM_AI_01_TYPERemovedDeviceRemovedReadings
            {
                RemovedRegisterID = "2",
                RegisterReading = (decimal)999.99
            };
            e.RemovedDevice = new GMREM_AI_01_TYPERemovedDevice
            {
                ActiveImportRegister = "1",
                AssetReusable = true,
                DeviceID = DeviceId,
                PrepaymentData = new GMREM_AI_01_TYPERemovedDevicePrepaymentData
                {
                    AccumulatedDebtRegister = "2",
                    EmergencyCreditBalance = "10",
                    MeterBalance = "100",
                    PaymentDebtRegister = "5",
                    TimeDebtRegister1 = "6",
                    TimeDebtRegister2 = "7"
                },
                ReadingDateTime = DateTime.Now,
                ReadingDateTimeSpecified = false,
                RemovedMSN = MSN,
                RemovedReadings = new GMREM_AI_01_TYPERemovedDeviceRemovedReadings[1] { r }
            };
            e.SupplierMPID = "SPARK";
            e.TransactionReference = "TRANS256";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.EMREM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "Siemens",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateGCHCV_IN_01ServiceRequest(string MPRN, string MSN)
        {
            //Service Request GCHCV_IN_01
            var e = new GCHCV_IN_01_TYPE();
            e.ImportMPRN = MPRN;
            e.CalorificValue = (decimal)39.2;
            e.CalorificValueSpecified = true;
            e.ConversionFactor = (decimal)1.02264;
            e.ConversionFactorSpecified = true;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.GCHCV,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXBCAL_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate)
        {
            //Service Request XBCAL_IN_01
            var e = new XBCAL_IN_01_TYPE();

            //This is another one that has two totally different routes depending on fuel type
            if (FuelType == "E")
            {
                var elec = new XBCAL_IN_01_TYPEImportMPAN();
                elec.ImportMPAN = MPXN;
                elec.MSN = MSN;
                elec.BillingTime = Convert.ToDateTime("00:00:00");
                elec.ItemElementName = ItemChoiceType70.BillingFrequencyMonthly;
                elec.Item = "01";
                e.Item = elec;
            }
            else
            {
                var gas = new XBCAL_IN_01_TYPEImportMPRN();
                gas.ImportMPRN = MPXN;
                gas.MSN = MSN;
                gas.BillingPeriodStart = Convert.ToDateTime(KeyDate + " 00:00:00");
                gas.Periodicity = XBCAL_IN_01_TYPEImportMPRNPeriodicity.Monthly;
                e.Item = gas;
            }
            e.DisplayMessage = "We changed your billing calendar, hurrah for us!";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XBCAL,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXPDET_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XPDET_IN_01
            var e = new XPDET_IN_01_TYPE();

            //This service request alters depending on whether it's electricity or gas
            if (FuelType == "E")
            {
                var elec = new XPDET_IN_01_TYPEElectricity();
                elec.ImportMPAN = MPXN;
                elec.MSN = MSN;
                var e1 = new XPDET_IN_01_TYPEElectricityElecDebtRecovery1();
                e1.DebtRecoveryRate = 500;
                e1.DebtRecoveryRatePeriod = XPDET_IN_01_TYPEElectricityElecDebtRecovery1DebtRecoveryRatePeriod.WEEKLY;
                e1.DebtRecoveryRatePriceScale = "-2";
                elec.ElecDebtRecovery1 = e1;
                var e2 = new XPDET_IN_01_TYPEElectricityElecDebtRecovery2();
                e2.DebtRecoveryRate = 0;
                e2.DebtRecoveryRatePeriod = XPDET_IN_01_TYPEElectricityElecDebtRecovery2DebtRecoveryRatePeriod.WEEKLY;
                e2.DebtRecoveryRatePriceScale = "-2";
                elec.ElecDebtRecovery2 = e2;
                e.Item = elec;
            }
            else
            {
                var gas = new XPDET_IN_01_TYPEGas();
                gas.ImportMPRN = MPXN;
                gas.MSN = MSN;
                var g1 = new XPDET_IN_01_TYPEGasGasDebtRecovery1();
                g1.DebtRecoveryRate = "71";
                g1.DebtRecoveryRatePeriod = XPDET_IN_01_TYPEGasGasDebtRecovery1DebtRecoveryRatePeriod.DAILY;
                gas.GasDebtRecovery1 = g1;
                var g2 = new XPDET_IN_01_TYPEGasGasDebtRecovery2();
                g2.DebtRecoveryRate = "0";
                g2.DebtRecoveryRatePeriod = XPDET_IN_01_TYPEGasGasDebtRecovery2DebtRecoveryRatePeriod.DAILY;
                gas.GasDebtRecovery2 = g2;
                e.Item = gas;
            }
            e.DebtRecoveryPerPayment = 0;
            e.PaymentDebtRegister = 0;
            e.TimeDebtRegister1 = 45000;
            e.TimeDebtRegister2 = 0;
            e.DisplayMessage = "Your debt amount of £450.00 has now been applied to your meter, as agreed";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XPDET,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCTAR_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string Mode, string KeyDate)
        {
            //Service Request XCTAR_IN_01
            var e = new XCTAR_IN_01_TYPE();
            e.DisplayMessage = "Welcome to Spark";

            //Electricity
            if (FuelType == "E")
            {
                var elec = new XCTAR_IN_01_TYPEElectricity();
                elec.ImportMPAN = MPXN;
                elec.MSN = MSN;
                elec.SSC = "0151";

                //Meter Tariffs
                var mt = new XCTAR_IN_01_TYPEElectricityMeterTariff();
                mt.ExecutionDateTime = Convert.ToDateTime(KeyDate);
                mt.StandingCharge = 1900;
                var r = new XCTAR_IN_01_TYPEElectricityMeterTariffRegisterDefinitions[2];
                var r1 = new XCTAR_IN_01_TYPEElectricityMeterTariffRegisterDefinitions();
                r1.RegisterID = "01";
                r1.TPR = "00210";
                r1.MeasurementQuantityID = "AI";
                r1.MeterRegisterMultiplier = "1";
                r1.RegisterPrice = "900";
                r[0] = r1;
                var r2 = new XCTAR_IN_01_TYPEElectricityMeterTariffRegisterDefinitions();
                r2.RegisterID = "02";
                r2.TPR = "00043";
                r2.MeasurementQuantityID = "AI";
                r2.MeterRegisterMultiplier = "1";
                r2.RegisterPrice = "500";
                r[1] = r2;
                mt.RegisterDefinitions = r;
                elec.MeterTariff = mt;

                //Billing Calendar
                var bc = new XCTAR_IN_01_TYPEElectricityBillingCalendar();
                bc.BillingTime = Convert.ToDateTime("00:00:00");
                bc.ItemElementName = ItemChoiceType74.BillingFrequencyMonthly;
                bc.Item = "01";
                elec.BillingCalendar = bc;

                //Schedule
                var s = new XCTAR_IN_01_TYPEElectricityScheduleElecSchedule[1] {
                    new XCTAR_IN_01_TYPEElectricityScheduleElecSchedule() {
                        ScheduleFrequency = XCTAR_IN_01_TYPEElectricityScheduleElecScheduleScheduleFrequency.Daily,
                        ServiceReferenceVariant = Mode == "Credit" ? ElecDSPScheduledServiceReferenceVariant.Item461 : ElecDSPScheduledServiceReferenceVariant.Item414,
                        ScheduleStartDate = Convert.ToDateTime(KeyDate),
                        ScheduleEndDateSpecified = false,
                        ScheduleExecutionStartTime = Convert.ToDateTime("00:00:00") }
                    };
                //This is how a delete would work, note it's a single instance, so we can't delete multiple schedules in one go
                var d = new XCTAR_IN_01_TYPEElectricitySchedule
                {
                    CreateSchedules = s
                };
                //var sd =
                //    new XCTAR_IN_01_TYPEElectricityScheduleDeleteSchedules() {
                //        DeleteSchedule = true,
                //        ScheduleID = "1"};
                //var d2 = new XCTAR_IN_01_TYPEElectricitySchedule
                //{
                //    DeleteSchedules = sd
                //};
                //elec.Schedule = d2;
                elec.Schedule = d;
                e.Item = elec;
            }

            //Gas
            if (FuelType == "G")
            {
                var gas = new XCTAR_IN_01_TYPEGas();
                gas.ImportMPRN = MPXN;
                gas.MSN = MSN;

                //Meter Tariff
                var mt = new XCTAR_IN_01_TYPEGasMeterTariff();
                mt.ExecutionDateTime = Convert.ToDateTime(KeyDate);
                mt.StandingCharge = 1900;
                var r = new XCTAR_IN_01_TYPEGasMeterTariffRegisterDefinitions[1];
                var r1 = new XCTAR_IN_01_TYPEGasMeterTariffRegisterDefinitions();
                r1.RegisterID = "01";
                r1.RegisterPrice = "100";
                r[0] = r1;
                mt.RegisterDefinitions = r;
                gas.MeterTariff = mt;

                //Billing Calendar
                var bc = new XCTAR_IN_01_TYPEGasBillingCalendar();
                bc.BillingPeriodStart = Convert.ToDateTime("00:00:00");
                bc.Periodicity = XCTAR_IN_01_TYPEGasBillingCalendarPeriodicity.Monthly;
                gas.BillingCalendar = bc;

                //Schedule
                var s = new XCTAR_IN_01_TYPEGasScheduleGasSchedule[1] {
                    new XCTAR_IN_01_TYPEGasScheduleGasSchedule() {
                        ScheduleFrequency = XCTAR_IN_01_TYPEGasScheduleGasScheduleScheduleFrequency.Daily,
                        ServiceReferenceVariant = Mode == "Credit" ? GasDSPScheduledServiceReferenceVariant.Item461 : GasDSPScheduledServiceReferenceVariant.Item414,
                        ScheduleStartDate = Convert.ToDateTime(KeyDate),
                        ScheduleEndDateSpecified = false,
                        ScheduleExecutionStartTime = Convert.ToDateTime("00:00:00") }
                    };
                var d = new XCTAR_IN_01_TYPEGasSchedule
                {
                    CreateSchedules = s
                };
                gas.Schedule = d;
                e.Item = gas;
            }

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCTAR,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXMBAL_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XMBAL_IN_01
            var e = new XMBAL_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType63.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType63.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;

            //We can either send a value as a string to adjust the meter balance by, or a boolean which will reset the balance
            e.Item1 = "20000"; //£0.20 adjustment
            //e.Item1 = true;
            e.DisplayMessage = "Enjoy your new balance";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XMBAL,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXDMSG_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XDMSG_IN_01
            var e = new XDMSG_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType80.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType80.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            //e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            //e.ExecutionDateTimeSpecified = true;
            e.DisplayMessage = "Hello, how are you doing?";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XDMSG,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXSARM_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XSARM_IN_01
            var e = new XSARM_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType83.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType83.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XSARM,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXSUPP_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate)
        {
            //Service Request XSUPP_IN_01
            var e = new XSUPP_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType81.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType81.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            //Don't use an execution time
            //e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.ExecutionDateTimeSpecified = false;
            e.SupplierName = "Spark";
            e.SupplierTelephoneNumber = "03333216246";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XSUPP,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCSCH_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string Mode, string KeyDate)
        {
            //Service Request XCSCH_IN_01
            var e = new XCSCH_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType71.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType71.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            e.CreateSchedule = new XCSCH_IN_01_TYPECreateSchedule[1]
            {
                new XCSCH_IN_01_TYPECreateSchedule()
                {
                    ExecutionStartTime = Convert.ToDateTime(KeyDate),
                    ServiceReferenceVariant = Mode == "Credit" ? DSPScheduledServiceReferenceVariant.Item461 : DSPScheduledServiceReferenceVariant.Item414,
                    Frequency = XCSCH_IN_01_TYPECreateScheduleFrequency.Monthly,
                    StartDate = Convert.ToDateTime(KeyDate),
                    EndDateSpecified = false
                }
            };
            e.DisplayMessage = "Your schedule has been updated";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCSCH,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCPAN_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XCPAN_IN_01
            var e = new XCPAN_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType93.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType93.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            e.PAN = "1234567890";
            e.PANOperation = XCPAN_IN_01_TYPEPANOperation.Create; //This ia a create operation as this only falls in a gain scenario in SIT, but in real life it could also be an update for a non-gain/ installation

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCPAN,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXPTOP_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XPTOP_IN_01
            var e = new XPTOP_IN_01_TYPE();
            e.ItemsElementName = new ItemsChoiceType11[3];
            e.ItemsElementName[0] = new ItemsChoiceType11();
            e.ItemsElementName[1] = new ItemsChoiceType11();
            e.ItemsElementName[2] = new ItemsChoiceType11();
            e.Items = new string[3];
            if (FuelType == "E")
            {
                e.ItemsElementName[0] = ItemsChoiceType11.ImportMPAN;
                e.Items[0] = MPXN;
            }
            else
            {
                e.ItemsElementName[0] = ItemsChoiceType11.ImportMPRN;
                e.Items[0] = MPXN;
            }
            e.ItemsElementName[1] = ItemsChoiceType11.MSN;
            e.Items[1] = MSN;
            e.ItemsElementName[2] = ItemsChoiceType11.PAN;
            e.Items[2] = "1234567890";
            e.TopUpAmount = "10000";
            e.DisplayMessage = "We topped you up, don't spend it all at once!";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XPTOP,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXPTOP_PI_IN_01ServiceRequest()
        {
            //Service Request XPTOP_PI_IN_01
            var e = new XPTOP_PI_IN_01_TYPE();
            e.InstructionCode = "INSTRUCTIONCODE";
            e.PAN = "PAN";
            e.ProcessingCode = "101";
            e.TopUpAmount = "200";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XPTOP,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "PayPoint",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXPTOP_PI_IN_03ServiceRequest()
        {
            //Service Request XPTOP_PI_IN_03
            var e = new XPTOP_PI_IN_03_TYPE();
            e.InstructionCode = "INSTRUCTIONCODE";
            e.PAN = "PAN";
            e.ProcessingCode = "101";
            e.TopUpAmount = "200";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XPTOP,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "PayPoint",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXCPRI_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate)
        {
            //Service Request XCPRI_IN_01
            var e = new XCPRI_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType96.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType96.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            var r = new XCPRI_IN_01_TYPERegister[1];
            var r1 = new XCPRI_IN_01_TYPERegister();
            r1.RegisterID = FuelType == "E" ? "01" : "01";
            r1.Price = "15000";
            r[0] = r1;
            e.Register = r;
            e.StandingCharge = 14000;
            e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.DisplayMessage = "Your tariff rate just changed!";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XCPRI,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXDPIN_IN_01ServiceRequest(string FuelType, string MPXN, string MSN)
        {
            //Service Request XDPIN_IN_01
            var e = new XDPIN_IN_01_TYPE();
            e.ItemElementName = FuelType == "E" ? ItemChoiceType102.ImportMPAN : ItemChoiceType102.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XDPIN,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXDPRN_IN_01ServiceRequest(string DeviceId)
        {
            //Service Request XDPRN_IN_01
            //Spark won't send this flow, but we need to pick up an exception triggered from it
            var e = new XDPRN_IN_01_TYPE();
            e.DeviceID = DeviceId;
            e.DeviceFirmwareVersion = "1.18";
            e.DeviceID = DeviceId;
            e.DeviceManufacturer = "Liberty";
            e.DeviceModel = "Sparkatron 2000";
            e.DeviceType = "Smart Meter";
            e.ESMEVariant = ESMEVariant.ADE;
            e.ESMEVariantSpecified = true;
            e.SMETSCHTSVersion = "1.21.9";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XDPRN,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateXPCON_IN_01ServiceRequest(string FuelType, string MPXN, string MSN, string KeyDate)
        {
            //Service Request XPCON_IN_01
            //Spark won't send this flow, but we need to pick up an exception triggered from it
            var e = new XPCON_IN_01_TYPE();
            if (FuelType == "E")
                e.ItemElementName = ItemChoiceType73.ImportMPAN;
            else
                e.ItemElementName = ItemChoiceType73.ImportMPRN;
            e.Item = MPXN;
            e.MSN = MSN;
            e.ExecutionDateTime = Convert.ToDateTime(KeyDate);
            e.DebtRecoveryRateCap = 14000; //£140.00
            e.EmergencyCreditLimit = "500";
            e.EmergencyCreditThreshold = "200";
            e.NonDisablementCalendarID = "0";
            e.MaximumCreditThreshold = "25000";
            e.MaximumMeterBalance = "100000";

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.XPCON,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }

        public BOLRequestType CreateETAMP_IN_01ServiceRequest(string MPAN, string MSN, string DeviceId)
        {
            //Service Request ETAMP_IN_01
            //Spark won't send this flow, but we need to pick up an exception triggered from it
            var e = new ETAMP_IN_01_TYPE();
            e.ImportMPAN = MPAN;
            e.ItemElementName = ItemChoiceType86.MSN;
            e.Item = MSN;
            e.SupplyTamperState = SupplyTamperStateType.Locked;

            //Create a new service request
            var br = new BOLRequestType
            {
                Header = new UtilisoftRequestHeaderType
                {
                    BusinessScenarioID = BusinessScenarioID.ETAMP,
                    PrecedingScenarioID = null,
                    Priority = null,
                    SourceID = "SparkEnergy",
                    SourceCounter = Guid.NewGuid().ToString()
                },
                Body = new BOLRequestTypeBody
                {
                    Items = new object[] { e }
                }
            };
            return br;
        }
    }
}
