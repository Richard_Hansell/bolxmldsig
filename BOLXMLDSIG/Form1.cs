﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;

namespace BOLXMLDSIG
{
    public partial class Form1 : Form
    {
        private string bolIP = @"http://172.20.122.115:6067/BolIn";
        private readonly MediaTypeHeaderValue XmlHeader = new MediaTypeHeaderValue("application/xml");
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
             Electricity.Checked = true;
            BolIPLabel.Text = "Target IP = " + bolIP;
        }

        private void SignXML_Click(object sender, EventArgs e)
        {
            //Sign the XML
            string xml = DigitallySignXML();

            //Now send the XML to the BOL
            HttpClient client = new HttpClient();
            var httpContent = new StringContent(xml, System.Text.Encoding.UTF8);
            httpContent.Headers.ContentType = XmlHeader;
            try
            {
                var response = client.PostAsync(bolIP, httpContent).Result;
                MessageBox.Show("XML Signed and sent to BOL! Message: {" + response.StatusCode.ToString() + "}");
            }
            catch (Exception exception) when (exception is System.Net.WebException ||
                                   exception is HttpRequestException ||
                                   exception is SocketException)
            {
                MessageBox.Show("Failed to send XML to BOL: " + exception.Message);
                return;
            }
            catch (AggregateException ae) when (ae is AggregateException)
            {
                MessageBox.Show("Failed to send XML to BOL: " + ae.Flatten().Message + " - " + ae.InnerException);
                return;
            }

            //Take a copy of the XML that was sent, giving it a better name
            Directory.CreateDirectory(@"C:\Development\Archive");
            File.Copy(@"C:\Development\signed.xml", @"C:\Development\Archive\" + "_" + DateTime.Now.ToString("yyyyMMdd_HH_mm_ss") + ".xml");
        }

        private void LoadTemplate_Click(object sender, EventArgs e)
        {
            var s = new ServiceRequest();
            BOLRequestType sr = null;
            string fuelType = Flow.Text.StartsWith("E") || Flow.Text.Contains("ELEC") ? "E" : "G";
            string keyDate = DateTime.Now.ToString("yyyy-MM-dd");
            string xml = "";

            //Get the test MPxNs
            string MPAN = "1234567890123";
            string MPANMSN = "ELECMSN001";
            string MPRN = "9876543210";
            string MPRNMSN = "GASMSN001";
            string DeviceId = "E6-04-AB-B1-C3-DD-EE-A5";
            string GPFId = "86-0B-AB-BB-C3-AC-EE-55";
            string CHFId = "86-0B-AB-BB-C3-AC-11-11";
            string PPMId = "EE-EE-EE-AA-EE-EE-EE-01";

            //Generate the XML
            string mode = "Credit";
            if (Flow.SelectedIndex == -1 || Flow.Text.Contains("<"))
            {
                xml = "No XML to generate";
            }
            else
            {
                if (Flow.Text == "ECOSG_IN_01")
                    sr = new Collections().CreateECOSG_IN_01ServiceRequest(MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "EINIM_IN_01")
                    sr = new Collections().CreateEINIM_IN_01ServiceRequest(MPAN, mode, keyDate);
                else if (Flow.Text == "GCOSG_IN_01")
                    sr = new Collections().CreateGCOSG_IN_01ServiceRequest(MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "GINIM_IN_01")
                    sr = new Collections().CreateGINIM_IN_01ServiceRequest(MPRN, mode, keyDate);
                else if (Flow.Text == "XCMOD_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXCMOD_IN_01ServiceRequest("E", MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "XCMOD_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXCMOD_IN_01ServiceRequest("G", MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "ECOSL_IN_01")
                    sr = new Collections().CreateECOSL_IN_01ServiceRequest();
                else if (Flow.Text == "GCOSL_IN_01")
                    sr = new Collections().CreateGCOSL_IN_01ServiceRequest();
                else if (Flow.Text == "ENABL_IN_01")
                    sr = new Collections().CreateENABL_IN_01ServiceRequest(MPAN, MPANMSN);
                else if (Flow.Text == "EICOM_AI_01")
                    sr = new Collections().CreateEICOM_AI_01ServiceRequest(MPAN, MPANMSN, mode, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "GICOM_AI_01")
                    sr = new Collections().CreateGICOM_AI_01ServiceRequest(MPRN, MPRNMSN, mode, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "EICOM_IN_04")
                    sr = new Collections().CreateEICOM_IN_04ServiceRequest(MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "GICOM_IN_04")
                    sr = new Collections().CreateGICOM_IN_04ServiceRequest(MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "XITTD_AI_01" && Electricity.Checked)
                    sr = new Collections().CreateXITTD_AI_01ServiceRequest("E", MPAN, MPANMSN, keyDate, DeviceId, GPFId, CHFId, PPMId);
                else if (Flow.Text == "XITTD_AI_01" && Gas.Checked)
                    sr = new Collections().CreateXITTD_AI_01ServiceRequest("G", MPRN, MPRNMSN, keyDate, DeviceId, GPFId, CHFId, PPMId);
                else if (Flow.Text == "XIPMD_AI_01" && Electricity.Checked)
                    sr = new Collections().CreateXIPMD_AI_01ServiceRequest("E", MPAN, MPANMSN, keyDate, DeviceId, GPFId, CHFId, PPMId);
                else if (Flow.Text == "XIPMD_AI_01" && Gas.Checked)
                    sr = new Collections().CreateXIPMD_AI_01ServiceRequest("G", MPRN, MPRNMSN, keyDate, DeviceId, GPFId, CHFId, PPMId);
                else if (Flow.Text == "ECOTN_IN_01")
                    sr = new Collections().CreateECOTN_IN_01ServiceRequest(MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "GCOTN_IN_01")
                    sr = new Collections().CreateGCOTN_IN_01ServiceRequest(MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "GCHCV_IN_01")
                    sr = new Collections().CreateGCHCV_IN_01ServiceRequest(MPRN, MPRNMSN);
                else if (Flow.Text == "XMBAL_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXMBAL_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XMBAL_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXMBAL_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XDMSG_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXDMSG_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XDMSG_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXDMSG_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XSARM_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXSARM_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XSARM_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXSARM_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XCTAR_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXCTAR_IN_01ServiceRequest("E", MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "XCTAR_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXCTAR_IN_01ServiceRequest("G", MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "XPDET_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXPDET_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XPDET_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXPDET_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XBCAL_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXBCAL_IN_01ServiceRequest("E", MPAN, MPANMSN, keyDate);
                else if (Flow.Text == "XBCAL_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXBCAL_IN_01ServiceRequest("G", MPRN, MPRNMSN, keyDate);
                else if (Flow.Text == "XSUPP_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXSUPP_IN_01ServiceRequest("E", MPAN, MPANMSN, keyDate);
                else if (Flow.Text == "XSUPP_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXSUPP_IN_01ServiceRequest("G", MPRN, MPRNMSN, keyDate);
                else if (Flow.Text == "XCSCH_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXCSCH_IN_01ServiceRequest("E", MPAN, MPANMSN, mode, keyDate);
                else if (Flow.Text == "XCSCH_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXCSCH_IN_01ServiceRequest("G", MPRN, MPRNMSN, mode, keyDate);
                else if (Flow.Text == "XCPAN_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXCPAN_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XCPAN_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXCPAN_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XPTOP_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXPTOP_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XPTOP_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXPTOP_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XCPRI_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXCPRI_IN_01ServiceRequest("E", MPAN, MPANMSN, keyDate);
                else if (Flow.Text == "XCPRI_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXCPRI_IN_01ServiceRequest("G", MPRN, MPRNMSN, keyDate);
                else if (Flow.Text == "XDPIN_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXDPIN_IN_01ServiceRequest("E", MPAN, MPANMSN);
                else if (Flow.Text == "XDPIN_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXDPIN_IN_01ServiceRequest("G", MPRN, MPRNMSN);
                else if (Flow.Text == "XDPRN_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXDPRN_IN_01ServiceRequest(DeviceId);
                else if (Flow.Text == "XDPRN_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXDPRN_IN_01ServiceRequest(DeviceId);
                else if (Flow.Text == "XPCON_IN_01" && Electricity.Checked)
                    sr = new Collections().CreateXPCON_IN_01ServiceRequest("E", MPAN, MPANMSN, keyDate);
                else if (Flow.Text == "XPCON_IN_01" && Gas.Checked)
                    sr = new Collections().CreateXPCON_IN_01ServiceRequest("G", MPRN, MPRNMSN, keyDate);
                else if (Flow.Text == "EVCOM_AI_01")
                    sr = new Collections().CreateEVCOM_AI_01ServiceRequest(MPAN, MPANMSN, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "GVCOM_AI_01")
                    sr = new Collections().CreateGVCOM_AI_01ServiceRequest(MPRN, MPRNMSN, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "XCHUB_AI_01")
                    sr = new Collections().CreateXCHUB_AI_01ServiceRequest(MPRN, MPRNMSN, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "XVALI_AI_01")
                    sr = new Collections().CreateXVALI_AI_01ServiceRequest(MPAN);
                else if (Flow.Text == "EMREM_AI_01")
                    sr = new Collections().CreateEMREM_AI_01ServiceRequest(MPAN, MPANMSN, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "GMREM_AI_01")
                    sr = new Collections().CreateGMREM_AI_01ServiceRequest(MPRN, MPRNMSN, keyDate, DeviceId, GPFId, CHFId);
                else if (Flow.Text == "XCANC_AI_01" && Electricity.Checked)
                    sr = new Collections().CreateXCANC_AI_01ServiceRequest("E", MPAN);
                else if (Flow.Text == "XCANC_AI_01" && Gas.Checked)
                    sr = new Collections().CreateXCANC_AI_01ServiceRequest("G", MPRN);
                else if (Flow.Text == "XCHRT_AI_01")
                    sr = new Collections().CreateXCHRT_AI_01ServiceRequest(DeviceId, GPFId, CHFId);
                else if (Flow.Text == "ETAMP_IN_01")
                    sr = new Collections().CreateETAMP_IN_01ServiceRequest(MPAN, MPANMSN, DeviceId);
                else if (Flow.Text == "XPTOP_PI_IN_01")
                    sr = new Collections().CreateXPTOP_PI_IN_01ServiceRequest();
                else if (Flow.Text == "XPTOP_PI_IN_03")
                    sr = new Collections().CreateXPTOP_PI_IN_03ServiceRequest();
                else
                    xml = "This service request hasn't been developed yet";
                if (xml == "")
                    xml = new ServiceRequest().ConvertToXML(sr);
            }
            XMLContainer.Text = xml;
        }

        private void ReloadFlows()
        {
            string currentItem = Flow.Text;
            Flow.Items.Clear();

            //Electricity only
            if (Electricity.Checked)
            {
                Flow.Items.Add("ECOSG_IN_01");
                Flow.Items.Add("EINIM_IN_01");
                Flow.Items.Add("ECOSL_IN_01");
                Flow.Items.Add("ENABL_IN_01");
                Flow.Items.Add("EICOM_AI_01");
                Flow.Items.Add("EINIM_IN_04");
                Flow.Items.Add("ECOTN_IN_01");
                Flow.Items.Add("EVCOM_AI_01");
                Flow.Items.Add("EMREM_AI_01");
                Flow.Items.Add("ETAMP_IN_01");
            }

            //Gas only
            if (Gas.Checked)
            {
                Flow.Items.Add("GCOSG_IN_01");
                Flow.Items.Add("GINIM_IN_01");
                Flow.Items.Add("GCOSL_IN_01");
                Flow.Items.Add("GCOSG_IN_01");
                Flow.Items.Add("GINIM_IN_01");
                Flow.Items.Add("GICOM_AI_01");
                Flow.Items.Add("GICOM_IN_04");
                Flow.Items.Add("GCOTN_IN_01");
                Flow.Items.Add("GCHCV_IN_01");
                Flow.Items.Add("GVCOM_AI_01");
                Flow.Items.Add("GMREM_AI_01");
            }

            //Both
            Flow.Items.Add("XCMOD_IN_01");
            Flow.Items.Add("XITTD_AI_01");
            Flow.Items.Add("XIPMD_AI_01");
            Flow.Items.Add("XMBAL_IN_01");
            Flow.Items.Add("XDMSG_IN_01");
            Flow.Items.Add("XSARM_IN_01");
            Flow.Items.Add("XCTAR_IN_01");
            Flow.Items.Add("XPDET_IN_01");
            Flow.Items.Add("XBCAL_IN_01");
            Flow.Items.Add("XPSUPP_IN_01");
            Flow.Items.Add("XCSCH_IN_01");
            Flow.Items.Add("XCPAN_IN_01");
            Flow.Items.Add("XPTOP_IN_01");
            Flow.Items.Add("XCPRI_IN_01");
            Flow.Items.Add("XDPIN_IN_01");
            Flow.Items.Add("XDPRN_IN_01");
            Flow.Items.Add("XPCON_IN_01");
            Flow.Items.Add("XCANC_AI_01");
            Flow.Items.Add("XVALI_AI_01");
            Flow.Items.Add("XCHRT_AI_01");
            Flow.Items.Add("XPTOP_PI_IN_01");
            Flow.Items.Add("XPTOP_PI_IN_03");

            //Try to pick the same flow, if it's still in the list
            for (int i = 0; i < Flow.Items.Count; i++)
            {
                if (Flow.Items[i].ToString() == currentItem)
                    Flow.SelectedIndex = i;
            }
        }

        private void Electricity_CheckedChanged(object sender, EventArgs e)
        {
            ReloadFlows();
        }

        private void Gas_CheckedChanged(object sender, EventArgs e)
        {
            ReloadFlows();
        }

        private string DigitallySignXML()
        {
            //We need to sign the XML, or it won't validate
            try
            {
                //Call a Java application to digitally sign the XML
                File.WriteAllText(@"C:\Development\unsigned.xml", XMLContainer.Text);
                File.Delete(@"C:\Development\signed.xml");
                using (var p = Process.Start("java.exe", @"-jar C:\Development\XMLSigTest.jar"))
                {
                    p.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("XML failed to sign: " + ex.Message);
                return null;
            }

            //Wait for 5 seconds to ensure the Java has released any locks on the file
            System.Threading.Thread.Sleep(5000);

            //Now load back the signed XML
            HttpClient client = new HttpClient();
            string xml;
            try
            {
                xml = File.ReadAllText(@"C:\Development\signed.xml");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to read back signed XML: " + ex.Message);
                return null;
            }
            return xml;
        }

        private void ValidateXML_Click(object sender, EventArgs e)
        {
            ValidateXMLContainer.Text = "Working...";
            string xml = DigitallySignXML();
            ValidateXMLContainer.Text = "";

            //Load the XSD
            var schemaSet = new XmlSchemaSet();
            schemaSet.Add("http://www.utilisoft.co.uk/namespaces/dccwb/UtilisoftBOL_1.0", "UtilisoftBOL_2.0.0.xsd");
            ValidateXMLString(xml, schemaSet);
            if (ValidateXMLContainer.Text == "")
                ValidateXMLContainer.Text = "No Errors Found!";
        }

        private void ValidateXMLString(string xml, XmlSchemaSet schemaSet)
        {
            try
            {
                //Compile the schema
                XmlSchema compiledSchema = null;
                foreach (XmlSchema schema in schemaSet.Schemas())
                {
                    compiledSchema = schema;
                }

                //Set the validation settings
                var settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas.Add(compiledSchema);
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

                //Load the (amended) DSIG Schema
                var settingsDsig = new XmlReaderSettings();
                settingsDsig.ValidationType = ValidationType.DTD;
                settingsDsig.DtdProcessing = DtdProcessing.Ignore;
                var readerDsig = XmlReader.Create("xmldsig-core-schema.xsd", settingsDsig);
                settings.Schemas.Add(null, readerDsig);

                //Create the XmlReader object
                var reader = XmlReader.Create(new StringReader(xml), settings);

                //Parse the file
                while (reader.Read()) ;
                return;
            }
            catch (Exception ex)
            {
                ValidateXMLContainer.Text += ex.Message;
            }
        }

        //Display any warnings or errors.
        private void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                ValidateXMLContainer.Text += "\tWarning: Matching schema not found.  No validation occurred." + args.Message;
            else
                ValidateXMLContainer.Text += "\tValidation error: " + args.Message;
        }
    }
}
